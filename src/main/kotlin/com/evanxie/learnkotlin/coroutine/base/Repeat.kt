package com.evanxie.learnkotlin.coroutine.base

import kotlinx.coroutines.*

fun main() = runBlocking<Unit>() {
    repeat(100000) { //启动10万个协程，若启动10万个线程，开销是难以想象的
        launch {
            delay(1000)
            print(".")
        }
    }
}