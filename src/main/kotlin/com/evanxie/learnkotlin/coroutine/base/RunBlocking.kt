package com.evanxie.learnkotlin.coroutine.base
import com.evanxie.learnkotlin.coroutine.util.logThread
import kotlinx.coroutines.*

fun main(args: Array<String>) {
    dispatcherIoRunBlocking()
}

fun defaultRunBlocking(){
    logThread("a")
    runBlocking {
        for (i in 0..10) {
            println("$i ${Thread.currentThread().name}")
            delay(100)
        }
    }
    logThread("b")
}

fun dispatcherIoRunBlocking(){
    logThread("a")
    runBlocking(Dispatchers.IO) {
        for (i in 0..10) {
            println("$i ${Thread.currentThread().name}")
            delay(100)
        }
    }
    logThread("b")
}
