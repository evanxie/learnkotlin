package com.evanxie.learnkotlin.coroutine.channel

import com.evanxie.learnkotlin.coroutine.util.logThread
import kotlinx.coroutines.*
import kotlinx.coroutines.channels.*
import kotlin.math.log

/**
 * 关闭通道、迭代通道
 * close可以关闭通道，表明没有更多的元素将会进入通道
 *
 */
fun main() = runBlocking {
    val channel = Channel<Int>()
    launch {
        for (x in 1..5) {
            logThread("start send $x")
            channel.send(x * x)
        }
        logThread("start close")
        channel.close() // we're done sending
    }
    // here we print received values using `for` loop (until the channel is closed)
    for (y in channel) {
        logThread("start iterate $y")
        println(y)
    }
    println("Done!")
}