package com.evanxie.learnkotlin.coroutine.channel

import com.evanxie.learnkotlin.coroutine.util.logThread
import kotlinx.coroutines.*
import kotlinx.coroutines.channels.*

/**
 *
 * 结果：
 *  2019-12-04T16:44:30.139Z [main] start send 1
    2019-12-04T16:44:30.140Z [main] start send 2
    1
    2019-12-04T16:44:30.142Z [main] start receive
    4
    2019-12-04T16:44:30.142Z [main] start receive
    2019-12-04T16:44:30.142Z [main] start send 3
    9
    Done!
 */
fun main() = runBlocking {
    val channel = Channel<Int>()
    launch {
        // 这里可能是消耗大量 CPU 运算的异步逻辑，我们将仅仅做 5 次整数的平方并发送
        for (x in 1..3) {
            logThread("start send $x")
            channel.send(x * x)
        }
    }
// 这里我们打印了 5 次被接收的整数：

    repeat(3) {
        logThread("start receive")
        println(channel.receive())
    }

    println("Done!")
}

