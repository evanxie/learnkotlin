package com.evanxie.learnkotlin.coroutine.conception

import com.evanxie.learnkotlin.coroutine.util.logThread
import kotlinx.coroutines.*

fun main(args: Array<String>) = runBlocking {
    logThread("start")
    suspendingPrint()
    logThread("end")
}

/**
 * 执行到挂起函数，实际上是将挂起函数中的协程的代码脱离当前线程
 * suspend修饰的函数不代表就会挂起或者切换线程，仅仅表示一个说明，让该函数只能在协程中调用
 * 只有在suspend中调用Kotlin协程框架里的挂起函数，才会挂起或切换线程
 *
 * 下面的例子说明，suspend修饰的函数，还是在主线程中执行，没有切换线程。
 * 可以在其中使用withContext实现挂起或切换线程。
 */
suspend fun suspendingPrint() {
    // delay(1999) // 若去掉注释，则suspend起效。
    println("Thread: ${Thread.currentThread().name}")
}