package com.evanxie.learnkotlin.coroutine.scope

import com.evanxie.learnkotlin.coroutine.util.logThread
import kotlinx.coroutines.*

/**
 * runBlocking和coroutineScope主要的不同之处在于后者在等待所有的子协程执行完毕时候并没有使当前的线程阻塞。
 * launch的完成不用等子协程的完成
 *
 * 两个概念要区分：协程执行完，和阻塞，不是完全等同的
 *
 * launch（挂起函数）:不阻塞、其执行完不用等内部子协程执行完
 * coroutineScope（挂起函数）:不阻塞、其执行完要等内部执行完
 * runBlocking(并非挂起函数)：阻塞、其执行完要等内部执行完。若runBlocking中有GlobalScope.launch，其不会等待它执行完。
 */
fun main() = runBlocking { // this: CoroutineScope
    launch {
        logThread("Task from runBlocking start")
        delay(1000L)
        logThread("Task from runBlocking end")
    }

    logThread("coroutineScope before")

    coroutineScope { // Creates a coroutine scope
        logThread("Task from coroutine scope start")
        launch {
            logThread("Task from nested launch start")
            delay(5000L)
            logThread("Task from nested launch end")
        }

        delay(150L)
        logThread("Task from coroutine scope end") // This line will be printed before the nested launch
    }

    logThread("Coroutine scope is over") // This line is not printed until the nested launch completes
}