package com.evanxie.learnkotlin.coroutine.scope

import com.evanxie.learnkotlin.coroutine.util.logThread
import kotlinx.coroutines.*

fun main() = runBlocking(Dispatchers.IO) {
    val job = launch {
        // 外层任务，包裹两个协程
        GlobalScope.launch {
            // 第一个协程
            for (i in 0..10) {
                logThread("GlobalScope $i -----")
                delay(100)
            }
        }

        launch {
            // 第二个协程
            for (i in 0..10) {
                logThread("normal launch $i #####")
                delay(100)
            }
        }
    }
    delay(300); // 延迟一会，让第二个协程能执行3次左右
    job.cancel() // 将外层任务取消了


    delay(2000) // 继续延迟，期望看到GlobalScope能继续运行. //注意，此处还有一个知识点，如果将改行注释，GlobalScope.launch的协程无法保活。就像守护进程类似，只有它一个时，就会被杀掉。
}
