package com.evanxie.learnkotlin.coroutine.util

import java.time.*

fun logThread(msg: String) = println("${Instant.now()} [${Thread.currentThread().name}] $msg")