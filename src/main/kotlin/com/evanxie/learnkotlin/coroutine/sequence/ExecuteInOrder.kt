package com.evanxie.learnkotlin.coroutine.sequence

import com.evanxie.learnkotlin.coroutine.util.logThread
import kotlinx.coroutines.*
import kotlin.system.measureTimeMillis

/**
 * 保证两个函数顺序执行
 * 不读两个函数的返回值，也是顺序执行
 */
fun main() = runBlocking {
    logThread("Mission start")
    val time = measureTimeMillis {
        val one = doSomething1()
        val two = doSomething2()
        logThread("The answer is ${one + two}") // 注释掉这两行，两个挂起函数也是顺序执行
    }
    logThread("Completed in $time ms")
}

suspend fun doSomething1(): Int {
    logThread("doSomething1 start")
    delay(1000)
    logThread("doSomething1 end")
    return 13
}

suspend fun doSomething2(): Int {
    logThread("doSomething2 start")
    delay(1000)
    logThread("doSomething2 end")
    return 27
}
