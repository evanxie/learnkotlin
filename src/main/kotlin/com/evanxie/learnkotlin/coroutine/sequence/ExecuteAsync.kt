package com.evanxie.learnkotlin.coroutine.sequence
import com.evanxie.learnkotlin.coroutine.util.logThread
import kotlinx.coroutines.*
import kotlin.system.measureTimeMillis


fun main() {
    asyncCoroutine()
    //lazyAsyncCoroutine()
}

fun asyncCoroutine() {
    runBlocking {
        logThread("Mission start")
        val time = measureTimeMillis {
            val one = async { doSomething1() }
            val two = async { doSomething2() }
            logThread("The answer is ${one.await() + two.await()}") // 若注释该句，则很快执行后面的语句，不会等待
        }
        logThread("Completed in $time ms")
    }
}

/**
 * 只有当结果需要被 await 或者如果一个 start 函数被调用，协程才会被启动
 *
 * 1.在one.start(),two.start()后，执行delay。会发现3个协程并行，delay时间最长。总时间为delay时间。
 * 2.注释delay，并行执行one和two，耗时1秒左右
 * 3.注释one.start()与two.start()，执行到await才启动协程。并且是先启动one协程，执行完以后，才启动two协程
 */
fun lazyAsyncCoroutine() {
    runBlocking {
        logThread("Mission start")
        val time = measureTimeMillis {
            val one = async(start = CoroutineStart.LAZY) { doSomething1() }
            val two = async(start = CoroutineStart.LAZY) { doSomething2() }
            //one.start()
            //two.start()
            delay(3000)
            logThread("The answer is ${one.await() + two.await()}")
        }
        logThread("Completed in $time ms")
    }
}
