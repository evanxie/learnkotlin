package com.evanxie.learnkotlin.coroutine.sequence

import com.evanxie.learnkotlin.coroutine.util.logThread
import javafx.application.Application.launch
import kotlinx.coroutines.*
import kotlin.system.measureTimeMillis

fun main() = runBlocking {
    //launchSyncOrAsync()
    launchSyncOrAsync()
    //dispatcherSyncOrAsync()
}

suspend fun launchSyncOrAsync(){
    coroutineScope  {
        launch { // 默认继承 parent coroutine 的 CoroutineDispatcher，指定运行在 main 线程
            logThread("coroutineScope 1 launch 1")
        }
        launch { // 默认继承 parent coroutine 的 CoroutineDispatcher，指定运行在 main 线程
            logThread("coroutineScope 1 launch 2")
        }
        logThread("coroutineScope 1 out launch")
        kotlinx.coroutines.delay(100)
    }
    // logThread("coroutineScope end1")

    coroutineScope {
        launch { // 默认继承 parent coroutine 的 CoroutineDispatcher，指定运行在 main 线程
            logThread("coroutineScope 2 launch 1")
        }
        launch { // 默认继承 parent coroutine 的 CoroutineDispatcher，指定运行在 main 线程
            logThread("coroutineScope 2 launch 2")
        }
        logThread("coroutineScope 2 out launch")
    }

}

/**
 *  互相包含的withContext之间，是同步阻塞的。即使跨越不同的线程
 *
 *  结果：
 *  2019-12-04T15:40:38.551Z [main] dispatcherSyncOrAsync before
    2019-12-04T15:40:38.644Z [DefaultDispatcher-worker-1] Dispatchers.Default before
    2019-12-04T15:40:39.155Z [DefaultDispatcher-worker-1] Dispatchers.IO before
    2019-12-04T15:40:40.160Z [DefaultDispatcher-worker-3] Dispatchers.IO end
    2019-12-04T15:40:40.163Z [DefaultDispatcher-worker-3] Dispatchers.Default end
    2019-12-04T15:40:40.163Z [main] dispatcherSyncOrAsync end
 */
suspend fun hierarchicalWithContextSyncOrAsync(){
    logThread("dispatcherSyncOrAsync before")
    withContext(Dispatchers.Default) {
        logThread("Dispatchers.Default before")
        kotlinx.coroutines.delay(500)
        withContext(Dispatchers.IO){
            logThread("Dispatchers.IO before")
            delay(1000)
            logThread("Dispatchers.IO end")
        }
        logThread("Dispatchers.Default end")
    }
    logThread("dispatcherSyncOrAsync end")
}

/**
 * 平行的withContext之间，是同步阻塞的。即使跨越不同的线程
 *
 * 结果：
    2019-12-04T15:51:04.821Z [DefaultDispatcher-worker-1] Dispatchers.Default 1
    2019-12-04T15:51:05.444Z [DefaultDispatcher-worker-3] Dispatchers.Default 2
    2019-12-04T15:51:06.452Z [DefaultDispatcher-worker-2] Dispatchers.IO
    2019-12-04T15:51:06.453Z [main] parallelWithContextSyncOrAsync end
 */
suspend fun parallelWithContextSyncOrAsync(){
    withContext(Dispatchers.Default) {
        logThread("Dispatchers.Default 1")
        kotlinx.coroutines.delay(500)
    }
    withContext(Dispatchers.Default) {
        logThread("Dispatchers.Default 2")
        kotlinx.coroutines.delay(1000)
    }
    withContext(Dispatchers.IO) {
        logThread("Dispatchers.IO")
    }
    logThread("parallelWithContextSyncOrAsync end")
}


/**
 *  平行并列的withTimeoutOrNull是同步阻塞的，若超时则返回Null
 *
 *  结果：
    1 I'm sleeping 0 ...
    1 I'm sleeping 1 ...
    1 I'm sleeping 2 ...
    2 I'm sleeping 0 ...
    2 I'm sleeping 1 ...
    2 I'm sleeping 2 ...
    2019-12-04T15:48:45.280Z [main] result1:null
    2019-12-04T15:48:45.348Z [main] result2:Done 2
 */
suspend fun parallelWithTimeoutSyncOrAsync(){
    val result1 = withTimeoutOrNull(1300L) {
        repeat(3) { i ->
            println("1 I'm sleeping $i ...")
            delay(500L)
        }
        "Done 1" // 在它运行得到结果之前取消它
    }
    val result2 = withTimeoutOrNull(1600L) {
        repeat(3) { i ->
            println("2 I'm sleeping $i ...")
            delay(500L)
        }
        "Done 2" // 在它运行得到结果之前取消它
    }
    logThread("result1:$result1")
    logThread("result2:$result2")
}

/**
 *
 * 官方说明：
 * runBlocking和coroutineScope主要的不同之处在于后者在等待所有的子协程执行完毕时候并没有使当前的线程阻塞。
 * launch的完成不用等子协程的完成
 * -------------------------------------------
 *
 * 两个概念要区分：协程执行完，和阻塞，不是完全等同的
 *
 *
 * launch（扩展函数）:         其执行完不用等内部子协程执行完（异步），不阻塞线程
 * coroutineScope（挂起函数:   其执行完要等内部执行完不阻塞（同步）、不阻塞线程
 * withContext(挂起函数）:     其执行完要等内部执行完不阻塞（同步）、不阻塞线程
 * withTimeout（挂起函数）:    其执行完要等内部执行完不阻塞（同步）、不阻塞线程
 * runBlocking(并非挂起函数):  其执行完要等内部执行完（同步），阻塞线程。若runBlocking中有GlobalScope.launch，其不会等待它执行完。
 */


