package com.evanxie.learnkotlin.coroutine.cancel

import kotlinx.coroutines.*

fun main(){

}

/**
 * 超时机制，用于取消协程
 * 使用withTimeout，超时时会抛出异常TimeoutCancellationException
 * 使用时，可以添加try catch
 */
fun timeoutWithException() = runBlocking{
    withTimeout(1300){
        repeat(1000) { i ->
            println("I'm sleeping $i ...")
            delay(500L)
        }
    }
}

/**
 * 超时机制，用于取消协程
 * 使用withTimeoutOrNull，超时时会返回Null代替抛出异常
 */
fun timeoutWithNull() = runBlocking{
    val result = withTimeoutOrNull(1300L) {
        repeat(3) { i ->
            println("I'm sleeping $i ...")
            delay(500L)
        }
        "Done" // 在它运行得到结果之前取消它
    }
    println("Result is $result")
}
