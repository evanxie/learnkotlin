package com.evanxie.learnkotlin.coroutine.cancel

import com.evanxie.learnkotlin.coroutine.base.dispatcherIoRunBlocking
import com.evanxie.learnkotlin.coroutine.util.logThread
import kotlinx.coroutines.*

fun main(args: Array<String>) {
    cancelCallFinally()
}

// 调用取消失败
fun cancelFailed(){
    val startTime = System.currentTimeMillis();
    runBlocking {
        val job = GlobalScope.launch {
            var nextPrintTime = startTime
            var i = 0
            while (i < 10) { // 一个执行计算的循环，只是为了占用CPU
                // 每秒打印消息两次
                if (System.currentTimeMillis() >= nextPrintTime) {
                    println("I'm sleeping ${i++} ...")
                    nextPrintTime += 500L
                }
            }
        }
        delay(1300L) // 在延迟之后结束程序
        println("I'm tired of waiting")
        job.cancelAndJoin()
        println("Now I can quit.")
    }
}

// 调用取消成功，用isActive判断
fun cancelSuccessfully(){
    val startTime = System.currentTimeMillis();
    runBlocking {
        val job = GlobalScope.launch {
            var nextPrintTime = startTime
            var i = 0
            while (i < 10) { // 一个执行计算的循环，只是为了占用CPU
                // 判断协程是否被取消
                if (!isActive) {
                    break;
                }
                if (System.currentTimeMillis() >= nextPrintTime) {
                    println("I'm sleeping ${i++} ...")
                    nextPrintTime += 500L
                }
            }
        }
        delay(1300L) // 在延迟之后结束程序
        println("I'm tired of waiting")
        job.cancelAndJoin()
        println("Now I can quit.")
    }
}

// 调用取消后，会调用finally
fun cancelCallFinally() = runBlocking{
    val job = launch {
        try {
            repeat(1000) { i ->
                println("I'm sleeping $i ...")
                delay(500L)
            }
        } finally {
            println("I'm running finally")
        }
    }
    delay(1300L) // 延迟一段时间
    println("main: I'm tired of waiting!")
    job.cancelAndJoin() // 取消该任务并且等待它结束
    println("main: Now I can quit.")
}