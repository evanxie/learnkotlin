package com.evanxie.learnkotlin.coroutine.dispatcher

import com.evanxie.learnkotlin.coroutine.util.logThread
import kotlinx.coroutines.*


/**
 * withContext的作用域，相对前后也是同步的。
 */
fun main() = runBlocking {
    logThread("before withContext")
    withContext(Dispatchers.IO) {
        logThread("before inner withContext")
        delay(2000)
        logThread("end inner withContext")
    }
    logThread("end withContext")
}