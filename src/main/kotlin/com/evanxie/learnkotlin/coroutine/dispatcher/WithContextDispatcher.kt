package com.evanxie.learnkotlin.coroutine.dispatcher

import com.evanxie.learnkotlin.coroutine.cancel.cancelCallFinally
import com.evanxie.learnkotlin.coroutine.util.logThread
import kotlinx.coroutines.*

fun main(args: Array<String>) {
    // 该例子不可直接运行，待修改
    //launchIoWithContext()
}

/**
 * 使用withContext，会在其参数的线程中启动协程，执行完毕后会自动切换回原来的线程
 * 对于需要频繁切换回去的场景，比较简洁
 */
fun launchIoWithContext() = runBlocking{
    launch(Dispatchers.Main) {      // 👈 在 UI 线程开始
        val image = withContext(Dispatchers.IO) {  // 👈 切换到 IO 线程，并在执行完成后切回 UI 线程
            getImage("imageId")                      // 👈 将会运行在 IO 线程
        }
        // item.setImageBitmap(image)             // 👈 回到 UI 线程更新 UI
    }
}


/**
 * 为了在IO线程中执行getImage的任务的协程，在调用前在IO线程启动协程
 * 执行完成后，又切换回主线程的协程
 * 该方式不够优雅
 */
fun launchIo() = runBlocking {
    coroutineScope{
        launch(Dispatchers.IO) {
            val image = getImage("imageId")
            launch(Dispatchers.Main) {
                // item.setImageBitmap(image)
            }
        }
    }
}

fun getImage(imageId:String){
    runBlocking {
        // 耗时任务
        delay(5000)
    }
}